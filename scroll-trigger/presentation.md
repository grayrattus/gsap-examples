### GSAP ScrollTriggers + Timelines + Tweens
![What we do in the shadows](https://www.thewrap.com/wp-content/uploads/2021/08/Jackie-Daytona-What-We-Do-in-the-Shadows-936x527.jpg)

---

# Tween

> A Tween is what does all the animation work - think of it like a high-performance property setter. You feed in targets (the objects you want to animate), a duration, and any properties you want to animate and when its playhead moves to a new position, it figures out what the property values should be at that point applies them accordingly.


---

```
gsap.to(".box", {rotation: 27, x: 100, duration: 1});
```
<span style="color: purple;">Kind of like react-spring</span>

---

# Timeline

> A Timeline is a powerful sequencing tool that acts as a container for tweens and other timelines, making it simple to control them as a whole and precisely manage their timing. Without Timelines, building complex sequences would be far more cumbersome because you'd need to use a delay for every animation. 

---

```
// WITHOUT Timelines (only using tweens with delays):
gsap.to("#id", {x: 100, duration: 1});
gsap.to("#id", {y: 50, duration: 1, delay: 1});      //wait 1 second
gsap.to("#id", {opacity: 0, duration: 1, delay: 2}); //wait 2 seconds
```


```
//WITH Timelines (cleaner, more versatile)
var tl = gsap.timeline({repeat: 2, repeatDelay: 1});
tl.to("#id", {x: 100, duration: 1});
tl.to("#id", {y: 50, duration: 1});
tl.to("#id", {opacity: 0, duration: 1});

// then we can control the whole thing easily...
tl.pause();
tl.resume();
tl.seek(1.5);
tl.reverse();
...
```

---

# Scroll Trigger

It's a GSAP Plugin that allow to make animations based on user scroll

---


Basic example

```
gsap.to(".box", {
  scrollTrigger: ".box", // start the animation when ".box" enters the viewport (once)
  x: 500
});
```

---

Advanced timeline example

```
let tl = gsap.timeline({
    // yes, we can add it to an entire timeline!
    scrollTrigger: {
      trigger: ".container",
      pin: true,   // pin the trigger element while active
      start: "top top", // when the top of the trigger hits the top of the viewport
      end: "+=500", // end after scrolling 500px beyond the start
      scrub: 1, // smooth scrubbing, takes 1 second to "catch up" to the scrollbar
      snap: {
        snapTo: "labels", // snap to the closest label in the timeline
        duration: {min: 0.2, max: 3}, // the snap animation should be at least 0.2 seconds, but no more than 3 seconds (determined by velocity)
        delay: 0.2, // wait 0.2 seconds from the last scroll event before doing the snapping
        ease: "power1.inOut" // the ease of the snap animation ("power3" by default)
      }
    }
  });

// add animations and labels to the timeline
tl.addLabel("start")
  .from(".box p", {scale: 0.3, rotation:45, autoAlpha: 0})
  .addLabel("color")
  .from(".box", {backgroundColor: "#28a92b"})
  .addLabel("spin")
  .to(".box", {rotation: 360})
  .addLabel("end");
```

---

Callbacks example

```
ScrollTrigger.create({
  trigger: "#id",
  start: "top top",
  endTrigger: "#otherID",
  end: "bottom 50%+=100px",
  onToggle: self => console.log("toggled, isActive:", self.isActive),
  onUpdate: self => {
    console.log("progress:", self.progress.toFixed(3), "direction:", self.direction, "velocity", self.getVelocity());
  }
});
```

---

Definitions are taken from [gsap website](https://greensock.com/docs/v3). My examples can be found under my [domain](http://gsap-examples.dziedziczak-artur.xyz) and [codeberg](https://codeberg.org/grayrattus/gsap-examples)

![What we do in the shadows](https://64.media.tumblr.com/d668ed9b1602e5ea1897839604065bbe/e7b01631aac47199-90/s540x810/fc573e91b511b490140f0e5c10a0589128759fd9.gifv)
